/**
 * Assets
 */
import logo from './../logo.svg';

/**
 * Components
 */
import Button from './../components/button/Button';

/**
 * Styles
 */
import './App.css';

const iteracion = ["04/04/2000", "05/04/2003", "09/02/1995"];

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <div className="btns">
          {
            iteracion.map((item) => <Button text={item}/>)
          }    
        </div>
      </header>
    </div>
  );
}

export default App;
