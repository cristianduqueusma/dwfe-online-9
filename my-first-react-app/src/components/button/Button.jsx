/**
 * Styles
 */
import './Button.css';

function Button(props) {
  console.log(props);
  let fullYear = new Date(props.text).getFullYear();
  return (
    <div className="button">
      {fullYear}
    </div>
  );
}

export default Button;