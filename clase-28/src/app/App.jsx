/**
 * Styles
 */
import './App.css';

/**
 * Components
 */
import Album from './../components/album/Album';

/**
 * Data Base
 */
import { albumes } from "./../DB/albumes";

function App() {
  return (
    <div className="App">
      {
        albumes.map((album) =>
          <Album 
            portada={album.portada}
            artista={album.artista}
            titulo={album.titulo}
            fecha={album.fecha}
            genero={album.genero}
          />
        )
      }
      
    </div>
  );
}

export default App;
