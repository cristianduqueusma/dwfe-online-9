import './Album.css';

function Album(props) {

  const handleCard = (e) => {
    //alert(props.artista);
    console.log(e);
  }

  return (
    <div className="album-container" onClick={handleCard}>
      <img height="100%" src={props.portada} alt="tf" />
      <div className="album-contenido">
        <div className="album-descripcion">
          <h3 className="album-titulo">{props.artista}</h3>
          <h4 className="album-subtitulo">{props.titulo}</h4>
          <h4 className="album-subtitulo">
            ({props.fecha}) - <span className="cronologia">Hace x años</span>
          </h4>
        </div>
        <div className="album-pie">
          <span className="genero">{props.genero}</span>
        </div>
      </div>
    </div>
  );
}

export default Album;