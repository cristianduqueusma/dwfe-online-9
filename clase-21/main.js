// Arreglos
/**
 * Notación []
 * Multiples tipos de valores
 * Indice o Posición => Tiene q ser un número
 */


let mySecondArray = [2];
 let myFirstArray = [12, "Hola", true, undefined, 34, NaN, 34];
 /* myFirstArray.splice(3, 2);
 myFirstArray.push("Flor");
 myFirstArray.shift();
 myFirstArray.unshift("Rox");
 myFirstArray.pop();  */

const addItemToArray = function(arr, item) {
    return arr.push(item);
}

addItemToArray(myFirstArray, "Hola desde Medellín");
addItemToArray(mySecondArray, "Hola Lau");

console.log(myFirstArray);
console.log(mySecondArray);
