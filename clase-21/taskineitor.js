let myTasking = [
    "Hacer la cama",
    "Lavar ropa"
];

const showArray = function(arr) {
    console.log(arr);
}

const addTask = function(arr, task) {
    return arr.push(task);
}

const editTask = function(arr, i, newTask) {
    arr[i] = newTask;
    return arr;
}

const deleteTask = function(arr, i, elementsToRemove = 1) {
    return arr.splice(i, elementsToRemove);
}

showArray(myTasking);
addTask(myTasking, "Planchar");
editTask(myTasking, 1, 34);
deleteTask(myTasking, 0, 2);