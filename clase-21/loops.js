// Ciclos/Bucles/Loops
// Tipos de ciclos => for, while, dowhile, forof, foreach, map

let array = [5, 5, 5, 4.5, 5];

/* for (let cata = array.length-1; cata >= 0; cata--) {
    console.log(array[cata]);
} */

const average = function() {
    let averageStudent = 0;
    for (let i = 0; i < array.length; i++) {
      averageStudent = array[i] + averageStudent;
    }

    //console.log(averageStudent);

    return averageStudent/array.length;
}

alert(average());

// i   i < array.length (5)
/* 0  true
   1  true
   2  true
   3  true
   4  false
    */