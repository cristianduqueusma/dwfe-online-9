/**
 * Dependencies
 */
import { useState } from 'react';

/**
 * Components
 */
import Card from './../components/card/Card';
import Select from './../components/select/Select';
import Button from './../components/button/Button';

/**
 * Styles
 */
import './App.css';

/**
 * Others
 */
import hotels from './../DB/hotels';
import countries from './../DB/countries';
import prices from './../DB/prices';

function App() {
  const [hotelsData, hotelsDataUpdate] = useState(hotels);

  /**
   * @method handleFilter
   * @description Method that filters depend of the user selection.
   * @param {Object} selected Object User selection
   */
  const handleFilter = (selected) => {
    const { type, value, id } = selected;
    const hotelsFilter = hotelsData.filter((hotel) => hotel[type] === value );
    console.log(hotelsFilter)
    hotelsDataUpdate(0 === id ? hotels : hotelsFilter);
  };

  const triggerBtn = () => {
    alert('Limpiar');
  };

  return (
    <div className="App">
      <Button txt='Limpiar' triggerBtn={triggerBtn}/>
      <div className="header">
        <Select data={countries} handleFilter={handleFilter}/>
        <Select data={prices} handleFilter={handleFilter}/>
      </div>
      <div className="cards">
        {
          hotelsData.map((hotel) => 
            <Card 
              country={hotel.country}
              price={hotel.price}
              rooms={hotel.rooms}
              key={hotel.id}
            />
          )
        }
      </div>
    </div>
  );
}

export default App;
