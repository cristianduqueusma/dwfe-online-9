const prices = [
  {
    id: 0,
    value: "Elija un precio",
    type: 'price'
  },
  {
    id: 1,
    value: '50',
    type: 'price'
  },
  {
    id: 2,
    value: '80',
    type: 'price'
  },
  {
    id: 3,
    value: '100',
    type: 'price'
  },
  {
    id: 4,
    value: '120',
    type: 'price'
  }
];

export default prices;