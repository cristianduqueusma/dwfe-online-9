const countries = [
  {
    id: 0,
    value: 'Elegir país',
    type: 'country'
  },
  {
    id: 1,
    value: 'Colombia',
    type: 'country'
  },
  {
    id: 2,
    value: 'Argentina',
    type: 'country'
  },
  {
    id: 3,
    value: 'Uruguay',
    type: 'country'
  }
];

export default countries;