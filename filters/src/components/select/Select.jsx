/**
 * Dependencies
 */
import { useState } from 'react';

function Select(props) {
  const { data, handleFilter } = props;
  const [option, optionUpdate] = useState(data[0].value);

  const handleSelect = (e) => {
    const currentType = data.find((item) => e.target.value === item.value );
    optionUpdate(e.target.value);
    handleFilter(currentType);
  };

  return(
    <select value={option} onChange={handleSelect}>
      { data.map((item) => <option key={item.id}>{item.value}</option>) }
    </select>
  )
}

export default Select;