import './Card.css';

function Card(props) {
  const { country, price, rooms } = props;
  return (
    <div className='card'>
      <p>País: {country}</p>
      <p>Precio: ${price}</p>
      <p>Habitaciones: {rooms}</p>
    </div>
  )
}

export default Card;