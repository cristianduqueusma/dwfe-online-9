function Button(props) {
  const { txt, triggerBtn } = props

  const handleBtn = () => {
    triggerBtn();
  };

  return (
    <button onClick={handleBtn}>{txt}</button>
  )
}

export default Button;