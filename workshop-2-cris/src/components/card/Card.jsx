/**
 * Dependencies
 */
import { useState } from 'react';

/**
 * Styles
 */
import './Card.scss';

/**
 * Assets
 */
import star from '../../assets/icons/star.svg';

function Card(props) {
  const { id, name, lastname, description, photo, currentVotes, handlerSort } = props;
  const [ votes, updateVotes ] = useState(currentVotes);

  /**
   * @method add
   * @description Method that adds to the voting card
   */
  const add = () => {
    updateVotes(votes + 1);
    handlerSort({
      id,
      votes: votes + 1
    });
  }

  /**
   * @method substract
   * @description Method that subtracts to the voting card
   */
  const substract = () => {
    updateVotes(votes - 1);
    handlerSort(votes - 1);
  }

  return (
    <div className="card">
      <img className="card-photo" src={photo} alt="Foto"/>
      <div className="card-information">
        <p className="card-information-name">{name} {lastname}</p>  
        <p className="card-information-description">{description}</p>
        <span className="card-information-voting">
          <img src={star} alt="star-icon"/>
          Votos: {votes}
        </span>
      </div>
      <div className="card-voting">
        <span onClick={add}>+</span>
        <span onClick={substract}>-</span>
      </div>
    </div>
  );
}

export default Card;