const parceros = [
  {
    id: 1,
    name: "Lau",
    lastname: "Bisaccia",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nisl elit, dapibus vel purus in, malesuada tincidunt est. Morbi eu felis ut erat mollis aliquam. Ut a cursus mauris, et sodales leo.",
    photo: require("../assets/photos/lau.png"),
    votes: 2
  },
  {
    id: 2,
    name: "Florencia",
    lastname: "Carpanetto",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nisl elit, dapibus vel purus in, malesuada tincidunt est. Morbi eu felis ut erat mollis aliquam. Ut a cursus mauris, et sodales leo.",
    photo: require("../assets/photos/flor.png"),
    votes: 1
  },
  {
    id: 3,
    name: "Catalina",
    lastname: "Restrepo",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nisl elit, dapibus vel purus in, malesuada tincidunt est. Morbi eu felis ut erat mollis aliquam. Ut a cursus mauris, et sodales leo.",
    photo: require("../assets/photos/cata.png"),
    votes: 1
  },
  {
    id: 4,
    name: "Miller",
    lastname: "Rocha",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nisl elit, dapibus vel purus in, malesuada tincidunt est. Morbi eu felis ut erat mollis aliquam. Ut a cursus mauris, et sodales leo.",
    photo: require("../assets/photos/miller.png"),
    votes: 2
  },
  {
    id: 5,
    name: "Maximiliano",
    lastname: "",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nisl elit, dapibus vel purus in, malesuada tincidunt est. Morbi eu felis ut erat mollis aliquam. Ut a cursus mauris, et sodales leo.",
    photo: require("../assets/photos/maxi.png"),
    votes: 1
  }
];

export default parceros