/**
 * Dependencies
 */
import { useState } from 'react';

/**
 * Components
 */
import Card from '../components/card/Card'

/**
 * Styles
 */
import './App.scss';

/**
 * Others
 */
import parceros from '../DB/parceros.js';

function App() {
  const sortCards = parceros.sort((a, b) => {
    if (a.votes > b.votes) { return -1; }
    if (a.votes < b.votes) { return 1; }
    return 0;
  });
  const [ parcerosState, updateParcerosState ] = useState(sortCards);
  const [, forceUpdate] = useState(0);

  /**
   * @method handlerSort
   * @description Method that sorts the cards
   *              dependnig of the votes
   */
  const handlerSort = (data) => {
    const foundCardModified =  parceros.find((parcero) => parcero.id === data.id );
    sortCards.forEach((parcero) => {
      if (parcero.id === foundCardModified.id) {
        parcero.votes = data.votes;
      }
    });

    const cards = parceros.sort((a, b) => {
      if (a.votes > b.votes) { return -1; }
      if (a.votes < b.votes) { return 1; }
      return 0;
    });

    updateParcerosState(cards);
    forceUpdate(n => !n);
  }

  return (
    <div className="App">
      <h1>El Parcero del Mes</h1>
      {
        parcerosState.map((parcero, i) => {
          return (
            <Card
              id={parcero.id}
              name={parcero.name}
              lastname={parcero.lastname}
              description={parcero.description}
              photo={parcero.photo.default}
              currentVotes={parcero.votes}
              key={parcero.id}
              handlerSort={handlerSort}
            />
          )
        })
      }
    </div>
  );
}

export default App;
