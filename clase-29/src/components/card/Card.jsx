import { useState } from 'react';


function Example(props) {
  // Declara una nueva variable de estado, la cual llamaremos “count”
  const [lau, setLau] = useState(props.maxi);
  const [favorite, setFavorite] = useState(false);

  const handleClick = () => {
    const contador = lau + 1;
    setLau(contador);
  }

  const handleFavorite = () => {    
    setFavorite(!favorite)
  }
  
  console.log(favorite)
  return (
    <div>
      <p>You clicked {lau} times</p>
      <button onClick={handleClick}>
        Click me
      </button>
      <span
        className={favorite ? 'green' : ''}
        onClick={handleFavorite}
      >
        {
          !favorite ? "No favorito" : "favorito"
        }
      </span>
    </div>
  );
}

export default Example;