import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const person = {
  name: "Estefa",
  age: 27
};

const numbers = [1, 3, 4];

ReactDOM.render(
  <React.StrictMode>
    <App data={person} lastname={"Usma"} numbers={numbers}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
