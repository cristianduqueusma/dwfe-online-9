// Declaración de función
const add = function(num1, num2) {
    if(typeof num1 === "number" && typeof num2 === "number") {
        return num1 + num2;
    } else {
        return "Tipos de datos incorrectos";
    }
    
}

// Ejecución de la función
alert(add(4, "Fff"));
/* alert(add(10, 6));
alert(add(1, 6));
alert(add(3, 16));
alert(add(3, 30)); */

/* function add() {

}

const add = () => { //arrow functions

} */