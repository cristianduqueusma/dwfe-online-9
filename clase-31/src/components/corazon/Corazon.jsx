/**
 * Dependencies
 */
import { useState } from 'react';

/**
 * Styles
 */
import './Corazon.css';

/**
 * Assets
 */
import corazonFull from './../../assets/corazon-full.svg';
import corazonRoto from './../../assets/corazon-roto.svg';

function Corazon(props) {
  const { roto, handleCountHeart } = props;
  const [corazon, actualizarCorazon] = useState(roto);

  const handleHeart = () => {
    actualizarCorazon(!corazon);
    handleCountHeart(!corazon);
  }

  return (
    <div
      onClick={handleHeart}
      className="corazon"
    >
      {
        corazon
          ? <img src={corazonRoto} alt="Corazón Roto"/>
          : <img src={corazonFull} alt="Corazón Full"/>
      }      
    </div>
  );
}

export default Corazon;