/**
 * Dependencies 
 */
import { useState } from 'react';

/**
 * Components
 */
import Corazon from './../components/corazon/Corazon';

/**
 * Styles
 */
import './App.css';

/**
 * Others
 */
import { corazones } from './../DB/corazones.js';

function App() {

  const initBreakHearts = corazones.filter((corazon) => corazon.roto === true );
  const [countHeart, updateCountHeart] = useState(initBreakHearts.length);

  const handleCountHeart = (breakHeart) => {
    let totalCount = breakHeart ? (countHeart + 1) : (countHeart - 1);
    updateCountHeart(totalCount);
  }

  return (
    <div className="App">
      <h1> Hay {countHeart} corazones rotos </h1>
      { 
        corazones.map((corazon) => 
          <Corazon roto={corazon.roto} key={corazon.id} handleCountHeart={handleCountHeart}/>
        )
      }
    </div>
  );
}

export default App;
