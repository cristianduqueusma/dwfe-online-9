let myTasking = [
    {
        id: 1,
        title: "Hacer la cama",
        completed: true
    },
    {
        id: 2,
        title: "Lavar la ropa",
        completed: false
    },
    {
        id: 3,
        title: "Dormir",
        completed: false
    },
    {
        id: 4,
        title: "Trotar",
        completed: true
    },
    {
        id: 5,
        title: "Trapear",
        completed: false
    }
];

const showTask = function(id) {
    for (let i = 0; i < myTasking.length; i++) {
        if(myTasking[i].id === id) {
            return console.log(myTasking[i]);
        }
    }
}

const addTask = function(task) {
    task.id = myTasking.length + 1;
    myTasking.push(task);
}

showTask(4);
addTask({
    title: "Cepillarme",
    completed: false
});

addTask({
    title: "Barrer",
    completed: true
});

console.log(myTasking);