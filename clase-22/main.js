// SOAP
// GRAPH QL
// REST FULL -> stateless

//Objetos

let a = 1, b = 2, c = 3;

let myFirstObject = {
    name: "Lau",
    lastname: "Ossa",
    age: "29",
    func: function() {
        console.log("myFirstObject", this);
    }
};

let myObjectFunction = function(a, b, c) {
    console.log("myObjectFunction", this)
    return {
        age: a,
        average: b,
        size: c
    }
}

//console.log(myObjectFunction(a, b, c));

myObjectFunction(a, b, c);
myFirstObject.func();