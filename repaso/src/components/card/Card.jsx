/**
 * Components
 */
import Button from './../button/Button';

/**
 * Styles
 */
import './Card.css';

function Card(props) {
  const {country, price, rooms} = props;

  /**
   * @method triggerBtn
   * @description Method that shows the hotel reserved information.
   */
  const triggerBtn = () => {
    alert(
      `Su hotel tiene la siguiente información
       País: ${country}
       Precio: ${price}
       Habitaciones: ${rooms}
      `
    );
  }

  return (
    <div className='card'>
      <p>Pais: {country}</p>
      <p>Precio: {price}</p>
      <p>Habitaciones: {rooms}</p>
      <Button txt='Reservar' triggerBtn={triggerBtn}/>
    </div>
  )
}

export default Card;