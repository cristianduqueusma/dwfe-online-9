/**
 * Styles
 */
import './Button.css';

function Button(props) {
  const { 
    txt,
    className = 'secondary',
    triggerBtn
  } = props;

  /**
   * @method handleBtn
   * @description Method that triggers the CTA
   */
  const handleBtn = () => {
    !!triggerBtn && triggerBtn();
  }

  return (
    <button onClick={handleBtn} className={className}>{txt}</button>
  )
}

export default Button;