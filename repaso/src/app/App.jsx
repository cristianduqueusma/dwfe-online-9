/**
 * Styles
 */
 import './App.css';

/**
 * Components
 */
import Card from './../components/card/Card';
import Button from './../components/button/Button';
import Date from './../components/date/Date';


/**
 * Others
 */
import hotels from './../DB/hotels';

function App() {

  /**
   * @method triggerBtn
   * @description Method that cleans the hotel cards.
   */
  const triggerBtn = () => {
    alert("Estoy limpiando todas las cards")
  }

  let date = new Date();
  console.log(date);

  return (
    <div className="app">
      <Date />
      <div className='cards'>
        {
          hotels.map((hotel) => 
            <Card 
              country={hotel.country}
              price={hotel.price}
              rooms={hotel.rooms}
            />
          )
        }
      </div>
      <Button txt='Limpiar' className='primary' triggerBtn={triggerBtn}/>
    </div>
  );
}

export default App;
