const hotels = [
  {
    id: 1,
    country: 'Colombia',
    price: '100',
    rooms: 3
  },
  {
    id: 2,
    country: 'Uruguay',
    price: '50',
    rooms: 2
  },
  {
    id: 3,
    country: 'Argentina',
    price: '80',
    rooms: 4
  },
  {
    id: 4,
    country: 'Argentina',
    price: '50',
    rooms: 2
  },
  {
    id: 5,
    country: 'Colombia',
    price: '120',
    rooms: 4
  },
  {
    id: 6,
    country: 'Colombia',
    price: '120',
    rooms: 4
  },
  {
    id: 7,
    country: 'Argentina',
    price: '50',
    rooms: 2
  },
  {
    id: 8,
    country: 'Colombia',
    price: '100',
    rooms: 3
  },
];

export default hotels;