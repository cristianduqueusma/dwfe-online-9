const api = {
  myFunction: () => {
    alert("Hola desde mi export")
  },
  students: [
    {
      name: "Cata",
      lastname: "Rodriguez",
      age: 26
    },
    {
      name: "Estefa",
      lastname: "Cabal",
      age: 22
    },
    {
      name: "Maxi",
      lastname: "Peña",
      age: 29
    },
    {
      name: "Guido",
      lastname: "Diaz",
      age: 29
    }
  ]
}

const myFunction = () => {
  alert("Hola desde mi export");
}


export default api;