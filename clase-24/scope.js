// Contexto

// Global Scope
let a = "Hola";


// Function Scope
const b = function() {
  let d = "mundo";
  console.log(d)
}

// Structure scope
const c = function() {
  const e = 23;
  if(true) {
    const f = 10;
  }
  console.log(f);
}

console.log(this.alert);
/* console.log(a);
b(); */
//c();

const maxiObject = {
  a: "Hola",
  b: "Munndo",
  c: () => {
    console.log(a);
  }
}

maxiObject.c();

