import myTasking from "./database.js";


const showTask = function(id) {
    for (let i = 0; i < myTasking.length; i++) {
        if(myTasking[i].id === id) {
            return console.log(myTasking[i]);
        }
    }
}

const addTask = function(task) {
    task.id = myTasking.length + 1;
    myTasking.push(task);
}

export default {showTask, addTask}