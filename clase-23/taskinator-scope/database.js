let myTasking = [
    {
        id: 1,
        title: "Hacer la cama",
        completed: true
    },
    {
        id: 2,
        title: "Lavar la ropa",
        completed: false
    },
    {
        id: 3,
        title: "Dormir",
        completed: false
    },
    {
        id: 4,
        title: "Trotar",
        completed: true
    },
    {
        id: 5,
        title: "Trapear",
        completed: false
    }
];

export default myTasking;