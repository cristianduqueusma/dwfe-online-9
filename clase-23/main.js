// Callbacks -> Una función pasada como parametro a otra funcion
// Closures -> Una función retornada por otra, y esa funcion tendra acceso a los valores de la primera aún sabiendo q su ejecución fue finalizada.

// HOF son funciones q tienen o callbacks o closures.

/* function a(c) {
  return function b(d) {
    return c+d;
  }
}

alert(a(2)(4)); */

let numbers = [1, 2, 3, 4, 5, 6];

let calcDouble = function(num) {
  return num * 2;
}

let isBiggerThanThree = function (num) {
  return (num % 2 !== 0);
}

let newNumbers = numbers.map(calcDouble);
let numbersBiggerThanThree = numbers.filter(isBiggerThanThree);

/* let newNumbers = numbers.map(function(item, i) {
  console.log(item, i);
  return item * 2;
}); */

/* numbers.filter(function(item, i) {
  console.log(item, i);
  return item > 3;
}); */

//let numbersBiggerThanThree = numbers.filter((item) => item > 3);

//console.log(newNumbers, numbers);
console.log(numbersBiggerThanThree);