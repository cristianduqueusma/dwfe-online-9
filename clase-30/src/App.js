import React, { useState } from "react";
import "./App.css";
import { albumes } from "./albumes";
import { genres } from "./genres";

export default function App() {
  const [currentAlbumes, currentAlbumesUpdate] = useState(albumes);
  const handleState = (currentAlbumes) => {
    currentAlbumesUpdate(currentAlbumes)
  }

  return (
    <div>
      <Select handleState={handleState}/>
      {currentAlbumes.map((album) => {
        return (
          <Album
            key={album.id}
            artista={album.artista}
            titulo={album.titulo}
            fecha={album.lanzamiento}
            portada={album.portada}
            genero={album.genero}
            agrupacion={album.agrupacion}
          />
        );
      })}
    </div>
  );
}

function Album(props) {
  return (
    <div className="album-container">
      <img height="100%" src={props.portada} alt="tf" />
      <div className="album-contenido">
        <div className="album-descripcion">
          <h3 className="album-titulo">{props.artista}</h3>
          <h4 className="album-subtitulo">{props.titulo}</h4>
          <h4 className="album-subtitulo">
            ({props.fecha}) - <span className="cronologia">Hace x años</span>
          </h4>
        </div>
        <div className="album-pie">
          <span className="genero">{props.genero}</span>
        </div>
      </div>
    </div>
  );
}

function Select(props) {

  const [genre, genreUpdate] = useState("default");

  const handlerFilterGenre = (e) => {
    let maxi = e.target.value;
    genreUpdate(e.target.value);
    const albumesFiltered = albumes.filter((album) => album.genero === genre);
    props.handleState(albumesFiltered);
  }

  return (
    <>
      <span>{genre}</span>
      <select value={genre} onChange={handlerFilterGenre}>
        {
          genres.map((flor) => <option value={flor.name}>{flor.nameShowed}</option>)
        }
      </select>
    </>
  )
}
