export const genres = [
  {
    id: "0",
    name: "default",
    nameShowed: "Selecciona un genero"
  },
  {
    id: "1",
    name: "pop",
    nameShowed: "Pop"
  },
  {
    id: "2",
    name: "rock",
    nameShowed: "Rock"
  },
  {
    id: "3",
    name: "soul",
    nameShowed: "Soul"
  }
];
